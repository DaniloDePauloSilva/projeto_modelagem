<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8" isELIgnored="false" %> 
  <%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<html>
<head>

<link rel="stylesheet" href="/bootstrap/css/bootstrap.css" />
<link rel="stylesheet" href="/css/fontawesome.css">
<script src="/jquery/jquery.js"></script>
<script src="/bootstrap/js/bootstrap.js"></script>
<script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>

</head>

<style>

	.titulo, .subtitulo{
		margin-top: 20px;
		margin-bottom: 20px;
	}
	
	.row{
		margin-top: 15px;
	}
	
	.col-3, .col-4, .col-6, .col-12{
		margin-top: 10px;
	}
	
	
	#modalAddIngrediente .modal-content, #modalAddPrato .modal-content {
    	
    	width: 750px !important;
    	margin-left: -125px;
	}
	
	#modalResultado .modal-content{
		width: 1000px;
    	margin-left: -235px;
	}
	
/* 	#btnAddIngredientePrato */
/* 	{ */
/* 		background-image: url('/open-iconic-master/svg/plus.svg'); */
/* 		background-repeat: no-repeat; */
/* 		background-position: center; */
/* 		color: white; */
/* 	} */

	#btnAddIngredientePrato{
		margin-top: 20px;
	    font-size: 25px;
	    width: 45px;
	    max-height: 40px !important;
	    padding-top: 0px;
    }
	
	#divListaEventos, #divListaClientes, divListaIngredientes{
		
		overflow-y: scroll;
		max-height: 400px !important;
	}
	
	#tabelaClientes tr td, #tabelaClientes tr th,
	#tabelaEventos tr td, #tabelaEventos tr th{
		margin: 10px;
		padding: 10px;
	}
	
	.qtdeNegativa{
		
		color: #ff0000 !important;
	}
	
</style>

<body>

<div class="container">

	<h1 class="titulo">Projeto - Modelagem e Simulação</h1>
	<div class="row">
		<div class="col-4">
			<label>Horário de Início do Serviço</label>
			<input id="txtHoraInicio" type="time" class="form-control form-control-sm">
		</div>
		<div class="col-4">
			<label>Horário de Fim do Serviço</label>
			<input id="txtHoraFim" type="time" class="form-control form-control-sm">
		</div>
		<div class="col-4"></div>
		
		<div class="col-4">
			<label>Quantidade de clientes no dia</label>
			<input id="txtQtdeClientes" type="number" class="form-control form-control-sm">
		</div>
	</div>
	<h3 class="subtitulo">Ingredientes</h3>
	<div class="row">
		<div class="col-4">
			<button id="btnAbrirModalIngrediente" class="btn btn-secondary">
				Adicionar Ingrediente
			</button>
		</div>
	</div>
	
	<div class="row">
		<table id="tabelaIngredientes" class="table">
			<thead>
				<tr>
					<td>Descrição</td>
					<td>Unidade de Medida</td>
					<td>Quantidade Inicial</td>
				</tr>
			</thead>
			<tbody>
			
			</tbody>
		</table>
	</div>
	
	<h3 class="subtitulo">Cardápio</h3>
	<div class="row">
		<div class="col-12">
			<button id="btnAbrirModalAddPrato" class="btn btn btn-secondary">
				Adicionar Prato
			</button>
		</div>
	</div>
	<div class="row">
		<div class="col-12">
			<table id="tabelaCardapio" class="table">
				
				<thead>
					<tr>	
						<td>
							Descrição
						</td>
						<td>
							Tempo de Preparo
						</td>
						<td>
							Ingredientes
						</td>
					</tr>
				</thead>
				<tbody>
				
				</tbody>
				
			</table>
		</div>
	</div>
	
	<div class="row">
		<div class="col-4">
			<button id="btnIniciarSimulacao" class="btn btn-primary">Iniciar Simulação</button>
		</div>
	</div>
	
</div>



<!-- MODAIS -->
<!-- INICIO INGREDIENTE -->
<div id="modalAddIngrediente" class="modal fade" tabindex="-1" role="dialog">
  <div class="modal-dialog" role="document">
  	<div class="modal-content">
  		<div class="modal-header">
        	<h5 class="modal-title">Adicionar Ingrediente</h5>
        	<button type="button" class="close" data-dismiss="modal" aria-label="Close">
          		<span aria-hidden="true">&times;</span>
        	</button>
      	</div>
      	<div class="modal-body">
        	<div class="row">
        		<div class="col-6">
        			<label>Descrição</label>
        			<input id="txtDescricaoAddIngrediente" type="text" class="form-control form-control-sm">
       			</div>
       			<div class="col-6">
       				<label>Unidade de Medida</label>
       				<select id="cmbUnidadeMedidaIngrediente" class="form-control form-control-sm">
       					
       				</select>
       			</div>
       			<div class="col-6">
       				<label>Quantidade Inicial De Estoque</label>
       				<input id="txtQtdeInicialIngrediente" type="number" class="form-control form-control-sm">
       			</div>
       			<div class="col-6">
       				<label>Tempo de Reposição De Estoque (minutos)</label>
       				<input id="txtTempoReposicaoIngrediente" type="number" class="form-control form-control-sm" disabled>
       			</div>
       			<div class="col-6">
       				<label>Quantidade de Reposição de Estoque</label>
       				<input id="txtQtdeReposicaoIngrediente" type="number" class="form-control form-control-sm" disabled>
       			</div>
       		</div>
        </div>
      
      	<div class="modal-footer">
        	<button id="btnCancelarIngrediente" type="button" class="btn btn-secondary">Cancelar</button>
        	<button id="btnAdicionarIngrediente" type="button" class="btn btn-primary">OK</button>
      	</div>
   	</div>
   </div>
 </div>
<!-- FIM INGREDIENTES -->

<!-- INICIO CARDÁPIO  -->
<div id="modalAddPrato" class="modal fade" tabindex="-1" role="dialog">
  <div class="modal-dialog" role="document">
  	<div class="modal-content">
  		<div class="modal-header">
        	<h5 class="modal-title">Adicionar Prato</h5>
        	<button type="button" class="close" data-dismiss="modal" aria-label="Close">
          		<span aria-hidden="true">&times;</span>
        	</button>
      	</div>
      	<div class="modal-body">
        	<div class="row">
        		<div class="col-6">
        			<label>Descrição</label>
        			<input id="txtDescricaoAddPrato" type="text" class="form-control form-control-sm">
       			</div>
       			<div class="col-6">
       				<label>Tempo de Preparo</label>
       				<input id="txtTempoPreparoPrato" type="number" class="form-control form-control-sm">
       			</div>
       			<div class="col-12">
       				<hr />
       			</div>
       			
       			<div class="col-6">
       				<label>Ingredientes</label>
       				<select id="cmbAddPratoIngrediente" class="form-control form-control-sm">
       					<option value="1">gramas (g)</option>
       					<option value="2">mililitro (mL)</option>
       				</select>
       			</div>
       			<div class="col-3">
       				<label>
       					Quantidade	
       				</label>
       				<input id="txtQtdeIngredientePrato" type="number" class="form-control form-control-sm">
     			</div>
     			<div class="col-3">
     				<label></label>
     				<button id="btnAddIngredientePrato" class="btn btn-secondary">
     					+
     				</button>
     			</div>
       			<div class="col-3"></div>
       			<div class="col-12">
       				<table id="tabelaAddPratoIngrediente" class="table">
       					<thead>
       						<tr>
      							<th>Ingrediente</th>
      							<th>Qtde</th>
      							<th></th>
   							</tr>
       					</thead>
       					<tbody>
       					</tbody>
       				</table>
       			</div>
       		</div>
        </div>
      
      	<div class="modal-footer">
        	<button id="btnCancelarPrato" type="button" class="btn btn-secondary">Cancelar</button>
        	<button id="btnAdicionarPrato" type="button" class="btn btn-primary">OK</button>
      	</div>
   	</div>
   </div>
 </div>


<!-- FIM CARDÁPIO -->


<div id="modalResultado" class="modal fade" tabindex="-1" role="dialog">
  <div class="modal-dialog" role="document">
  	<div class="modal-content">
  		<div class="modal-header">
        	<h4>Resultado da Simulação</h5>
        	<button type="button" class="close" data-dismiss="modal" aria-label="Close">
          		<span aria-hidden="true">&times;</span>
        	</button>
      	</div>
      	<div class="modal-body">
     		
     		<div class="row">
     			<div class="col-12">
     				<label>Média de Tempo em Fila</label>
     				<input id="txtMediaTempoFila" class="form-control form-control-sm" type="text" readonly>
     			</div>
     			<div class="col-12">
     				<label>Número Máximo de Clientes na Fila</label>
     				<input id="txtNumMaxCliFila" class="form-control form-control-sm" type="text" readonly>
     			</div>
     			<div class="col-12">
		     		<div id="graficoFila">
		     			
	     			</div>	
     			</div>
     			<div id="divListaClientes" class="col-12">
     				<h5>Dados dos Clientes</h4>
     				<table id="tabelaClientes">
     					<thead>
     						<tr>
     							<th>ID</th>
     							<th>Tempo de Chegada (min.)</th>
     							<th>Tempo de Atendimento (min.)</th>
     							<th>Tempo de Espera em Fila (min.)</th>
     							<th>Pedido</th>
     						</tr>
     					</thead>
     					<tbody>
     					
     					</tbody>
     				</table>	
     			</div>
     			
     			<div id="divListaIngredientes" class="col-12">
     				<h5>Ingredientes</h5>
     				<table id="tabelaIngredientesSaida" class="table">
     					<thead>
   							<tr>
	     						<th>Descrição</th>
	     						<th>Qtde Inicial</th>
	     						<th>Qtde Final</th>
     						</tr>
   						</thead>
   						<tbody>
   						
   						</tbody>
     				</table>
     			</div>
     			
     			<div id="divListaEventos" class="col-12">
     				<h5>Eventos</h5>
     				<table id="tabelaEventos" class="table">
     					<thead>
     						<tr>
								<th>Instante</th>
								<th>Evento</th>     						
     						</tr>
     					</thead>
     					<tbody>
     					
     					</tbody>
     				</table>
     			</div>
     			
     		</div>
     		
     		   	
        </div>
      	<div class="modal-footer">
        	
      	</div>
   	</div>
   </div>
 </div>


<!-- FIM MODAIS -->

<script>

	google.charts.load('current', {'packages':['corechart']});
	

	var retorno = null;

	function enviarPost(url, json, callback)
	{
		$.ajax({
			  url:url,
			  type:"POST",
			  data:JSON.stringify(json),
			  contentType:"application/json",
			  dataType:"json",
			  success: callback
			})
	}

	$('#btnIniciarSimulacao').on('click', function(){
		
		var dadosSimulacao = retornaObjetoDadosSimulacao();
		
		if(validaEnvio())
		{
			enviarPost('${ pageContext.request.contextPath }/simulacao', dadosSimulacao, function(dadosRetorno){
				console.log("retorno: ");
				console.log(dadosRetorno);
				
				modalResultado.abrirModal(dadosRetorno);
			});
		}
	});

	$('#btnAbrirModalIngrediente').on('click', function(){
		
		funcoesModalIngrediente.abrirModal();
		
	});
	
	$('#btnCancelarIngrediente').on('click', function(){

		funcoesModalIngrediente.fecharModal();
		
	});
	
	$('#btnAdicionarIngrediente').on('click', function(){
		
		if(funcoesModalIngrediente.validaAdicionarIngrediente())
		{
			funcoesModalIngrediente.adicionarIngrediente();
			funcoesModalIngrediente.fecharModal();
		}
	});
	
	$('#btnAbrirModalAddPrato').on('click', function(){
		
		funcoesModalCardapio.abrirModal();
		
	});
	
	$('#btnAddIngredientePrato').on('click', function(){
		
		funcoesModalCardapio.adicionarIngrediente();
		
	});
	
	$('#btnAdicionarPrato').on('click', function(){
		
		if(funcoesModalCardapio.validaAdicionarPrato())
		{
			funcoesModalCardapio.adicionarPrato();
		}
		
	});
	
	var modelo = {
		listaUnidadesMedidas: [
			{
				sigla: "g",
				descricao: "gramas"
			},
			{
				sigla: "mL",
				descricao: "mililitros"
			}
		],
		listaIngredientes: [],
		cardapio: [],
		listaIngredientesModalPrato: []
	};
	
	
	function atualizarTabelaCardapio()
	{
		$('#tabelaCardapio tbody').empty();
		
		for(var i = 0; i < modelo.cardapio.length; i++)
		{
			console.log(i);
			var ingredientesView = retornaStringIngredientes(modelo.cardapio[i]);

			var linha = $(`
				<tr>
					<td>\${modelo.cardapio[i].descricao}</td>
					<td>\${modelo.cardapio[i].tempoPreparo} minutos</td>
					<td>\${ingredientesView}</td>
					<td><button onclick="removerPrato(\${i})" class="btn btn-secondary">remover</button></td>
				</tr>
					
			`);
			
			$('#tabelaCardapio tbody').append(linha);
		}
	}
	
	function retornaStringIngredientes(prato)
	{
		var retorno = "";
		
		for(var i = 0; i < prato.listaIngredientes.length; i++)
		{
			retorno += prato.listaIngredientes[i].descricao + " - " +
				prato.listaIngredientes[i].qtde + " " +
				prato.listaIngredientes[i].unidadeMedida.sigla;
			
			if(i != prato.listaIngredientes.length - 1)
				retorno += ", ";
		}
		
		return retorno;
	}

	
	function atualizarTabelaIngredientes()
	{
		$('#tabelaIngredientes tbody').empty();
		
		for(var i = 0; i < modelo.listaIngredientes.length; i++)
		{
			var qtdeInicialView = modelo.listaIngredientes[i].qtdeInicial + " " +
				modelo.listaIngredientes[i].unidadeMedida.sigla;
			
			var linha = $(`
				<tr>
					<td>\${modelo.listaIngredientes[i].descricao }</td>
					<td>\${modelo.listaIngredientes[i].unidadeMedida.descricao}</td>
					<td>\${qtdeInicialView}</td>
					<td>
						<button onclick="removerIngrediente(\${i})" class="btn btn-secondary">
							remover
						</button>
					</td>
				</tr>	
			`);
			
			$('#tabelaIngredientes tbody').append(linha);
		}
		
	}
	
	function removerPrato(idPrato){
		
		modelo.cardapio.splice(idPrato, 1);
		
		atualizarTabelaCardapio();
		
	}
	
	function removerIngrediente(idIngrediente)
	{
		var ingrediente = modelo.listaIngredientes.splice(idIngrediente, 1)[0];
		
		removerIngredienteDoPrato(ingrediente);
		atualizarTabelaIngredientes();
	}
	
	function removerIngredienteDoPrato(ingrediente)
	{
		for(var i = 0; i < modelo.cardapio.length; i++)
		{
			for(var j = 0; j < modelo.cardapio[i].listaIngredientes.length; j++)
			{
				if(modelo.cardapio[i].listaIngredientes[j].descricao == ingrediente.descricao)
				{
					modelo.cardapio[i].listaIngredientes.splice(j, 1);
					
					if(modelo.cardapio[i].listaIngredientes.length == 0)
					{
						modelo.cardapio.splice(i, 1);
						i--;
						break;
					}
				}
			}
		}
		
		atualizarTabelaCardapio();
	}
	
	const modalResultado = {
			
		abrirModal: function(dadosResultado)
		{
			modalResultado.preencherDadosView(dadosResultado);
			
			$('#modalResultado').modal('show');
			
		},
		limparModal: function(){
			$('#tabelaClientes tbody').empty();
			$('#tabelaEventos tbody').empty();
			$('#tabelaIngredientesSaida tbody').empty();
		},
		preencherDadosView: function(dadosResultado){
			
			retorno = dadosResultado;
			
			modalResultado.limparModal();
			
			$('#txtMediaTempoFila').val(dadosResultado.mediaTempoEmFila + " minutos");
			$('#txtNumMaxCliFila').val(dadosResultado.tamanhoMaximoDaFila.qtde);
			modalResultado.adicionarLinhasTabelaClientes(dadosResultado.clientesAtendidos);
			modalResultado.adicionarLinhasTabelaEventos(dadosResultado.listaEventos);
			modalResultado.adicionarLinhasTabelaIngredientes(dadosResultado.listaIngredientesSaida);
			modalResultado.desenharGraficoFila(dadosResultado.listaQtdeClientesEmFila);
			
			
		},
		adicionarLinhasTabelaIngredientes: function(listaIngredientes){
			
			for(var i = 0; i < listaIngredientes.length; i++)
			{
				var qtdeInicialView = listaIngredientes[i].qtdeInicial + " " + listaIngredientes[i].unidadeMedida.sigla;
				var qtdeFimView = listaIngredientes[i].qtde + " " + listaIngredientes[i].unidadeMedida.sigla;
				
				var linha = $(`
					<tr>
						<td>\${listaIngredientes[i].descricao}</td>
						<td>\${qtdeInicialView}</td>
						<td>\${qtdeFimView}</td>
					</tr>
				`);
				
				if(listaIngredientes[i].qtde < 0)
				{
					linha.addClass("qtdeNegativa");
				}
				
				$('#tabelaIngredientesSaida').append(linha);
			}
			
		},
		adicionarLinhasTabelaClientes: function(listaDadosClientes){
			
			for(var i = 0; i < listaDadosClientes.length; i++)
			{
				var tempoDeEsperaEmFila = listaDadosClientes[i].minutoDeAtendimento - listaDadosClientes[i].minutoDeChegada;
				
				var linha = $(`
					<tr>
						<td>\${listaDadosClientes[i].id}</td>
						<td>\${listaDadosClientes[i].minutoDeChegada}</td>
						<td>\${listaDadosClientes[i].minutoDeAtendimento}</td>
						<td>\${tempoDeEsperaEmFila}</td>
						<td>\${listaDadosClientes[i].pedido.descricao}</td>
					</tr>		
				`);
				
				$('#tabelaClientes tbody').append(linha);
			}
		},
		adicionarLinhasTabelaEventos: function(listaDadosEventos){
			
			for(var i = 0; i < listaDadosEventos.length; i++)
			{
				var linha = $(`
					<tr>
						<td>\${listaDadosEventos[i].instante}</td>
						<td>\${listaDadosEventos[i].descricao}</td>
					</tr>		
				`);
				
				$('#tabelaEventos tbody').append(linha);
			}
		},
		desenharGraficoFila: function(listaQtdeClientesEmFila){
			
			
			
			var grafico = new google.visualization.LineChart(document.getElementById("graficoFila"));
			
			var tabela = modalResultado.retornaDataTableGrafico(listaQtdeClientesEmFila);
			
	   	    var options = {
	          title: 'Qtde. de Clientes na Fila',
	          width: 850,
	          height: 500
	          
	          
	        };
			
			grafico.draw(tabela, options);
			
		},
		retornaDataTableGrafico: function(listaQtdeClientesEmFila){
			
			var tabela = new google.visualization.DataTable();
			
			tabela.addColumn('number', 'instante');
			tabela.addColumn('number', 'qtde');
			
			var linhas = [];
			
			for(var i = 0; i < listaQtdeClientesEmFila.length; i++)
			{
				console.log("instante: " + listaQtdeClientesEmFila[i].instante);
				console.log("qtde: " + listaQtdeClientesEmFila[i].qtde);
				linhas.push([listaQtdeClientesEmFila[i].instante, listaQtdeClientesEmFila[i].qtde]);
			}
			
			tabela.addRows(linhas);
			
			console.log(tabela)
			
			return tabela;
		}
	};
	
	
	const funcoesModalCardapio = {
		
		abrirModal: function(){
			$('#modalAddPrato').modal('show');
			funcoesModalCardapio.limparModal();
			funcoesModalCardapio.carregarComboIngredientes();
			
		},
		fecharModal: function(){
			$('#modalAddPrato').modal('hide');
		},
		limparModal: function(){
			
			modelo.listaIngredientesModalPrato = [];
			
			$('#txtDescricaoAddPrato').val('');
			$('#txtTempoPreparoPrato').val('');
			$('#cmbAddPratoIngrediente').empty();
			$('#txtQtdeIngredientePrato').val('');
			$('#tabelaAddPratoIngrediente tbody').empty();
		
		},
		carregarComboIngredientes: function(){
			
			$('#cmbAddPratoIngrediente').empty();
			
			$('#cmbAddPratoIngrediente').append(`
				<option value="">SELECIONE</option>	
			`);
			
			for(var i = 0; i < modelo.listaIngredientes.length; i++)
			{
				var opt = $(`
					<option value="\${i}">\${modelo.listaIngredientes[i].descricao}</option>		
				`);
				
				$('#cmbAddPratoIngrediente').append(opt);
			}
			
		},
		atualizarTabelaIngredientesPrato: function(){
			
			$('#tabelaAddPratoIngrediente tbody').empty();
			
			for(var i = 0; i < modelo.listaIngredientesModalPrato.length; i++)
			{
				var qtdeView = modelo.listaIngredientesModalPrato[i].qtde + " " + 
					modelo.listaIngredientesModalPrato[i].unidadeMedida.sigla
				var linha = $(`
					<tr>
						<td>\${modelo.listaIngredientesModalPrato[i].descricao}</td>
						<td>\${qtdeView}</td>
						<td><button onclick="funcoesModalCardapio.removerIngrediente(\${i})" class="btn btn-secondary">remover</button></td>
					</tr>
							
				`);
				
				$('#tabelaAddPratoIngrediente tbody').append(linha);
			}
			
		},
		removerIngrediente: function(indiceRemocao){
			
			modelo.listaIngredientesModalPrato.splice(indiceRemocao, 1);
			funcoesModalCardapio.atualizarTabelaIngredientesPrato();
			
		},
		adicionarIngrediente: function(){
			
			var ingredientePrato = {};
			
			ingredientePrato = clonarObjeto(modelo.listaIngredientes[parseInt($('#cmbAddPratoIngrediente').val())]);
			ingredientePrato.qtde = parseInt($('#txtQtdeIngredientePrato').val());
			
			modelo.listaIngredientesModalPrato.push(ingredientePrato);
			
			funcoesModalCardapio.atualizarTabelaIngredientesPrato();
			
		},
		validaAdicionarPrato: function(){
			
			var retorno = true;
			var mensagem = "";
			
			if($('#txtDescricaoAddPrato').val() == null || $('#txtDescricaoAddPrato').val() == "")
			{
				retorno = false;
				mensagem += "- DIGITE A DESCRIÇÃO DO PRATO\n";
			}
			
			if($('#txtTempoPreparoPrato').val() == null || $('#txtTempoPreparoPrato').val() == "")
			{
				retorno = false;
				mensagem += "- DIGITE O TEMPO DE PREPARO\n";
			}
			else if(isNaN($('#txtTempoPreparoPrato').val()))
			{
				retorno = false;
				mensagem += "- TEMPO DE PREPARO INVÁLIDO\n";
			}
			
			if(modelo.listaIngredientesModalPrato == null || modelo.listaIngredientesModalPrato.length == 0)
			{
				retorno = false;
				mensagem += "- NENHUM INGREDIENTE FOI ADICIONADO";
			}
			
			if(!retorno)
			{
				alert("VERIFIQUE OS CAMPOS: \n\n" + mensagem);
			}
			
			return retorno;
			
		},
		adicionarPrato: function()
		{
			var prato = {};
			
			prato.descricao = $('#txtDescricaoAddPrato').val();
			prato.tempoPreparo = parseInt($('#txtTempoPreparoPrato').val());
			prato.listaIngredientes = clonarObjeto(modelo.listaIngredientesModalPrato);
			
			
			modelo.cardapio.push(prato);
			modelo.listaIngredientesModalPrato = [];
			atualizarTabelaCardapio();
			
			funcoesModalCardapio.fecharModal();
			
		}
		
			
	};
	
	const funcoesModalIngrediente = {
			
		abrirModal: function(){
			funcoesModalIngrediente.carregarComboUnidadesMedidas();
			$('#modalAddIngrediente').modal('show');
		},
		fecharModal: function(){
			console.log("fecharModal");
			funcoesModalIngrediente.limparCamposModal();
			$('#modalAddIngrediente').modal('hide');
		},
		limparCamposModal: function(){
			$('#txtDescricaoAddIngrediente').val('');
			$('#cmbUnidadeMedidaIngrediente').val('');
			$('#txtQtdeInicialIngrediente').val('');
			
			$('#txtQtdeReposicaoIngrediente').val('');
			
		},
		carregarComboUnidadesMedidas: function(){
			
			$('#cmbUnidadeMedidaIngrediente').empty();
			
			$('#cmbUnidadeMedidaIngrediente').append(`
				<option value="">SELECIONE</option>		
			`);
			
			for(var i = 0; i < modelo.listaUnidadesMedidas.length; i++)
			{
				var opt = $(`
					<option value="\${i}">\${modelo.listaUnidadesMedidas[i].descricao} (\${modelo.listaUnidadesMedidas[i].sigla})</option>	
				`);
				
				$('#cmbUnidadeMedidaIngrediente').append(opt);
			}
			
		},
		formularioValido: function(){
			return true
		},
		adicionarIngrediente: function(){
			
			var ingrediente = funcoesModalIngrediente.retornaIngredienteDoForm();
			
			modelo.listaIngredientes.push(ingrediente);
			atualizarTabelaIngredientes();
			
		},
		validaAdicionarIngrediente: function(){
			
			var retorno = true;
			var mensagem = "";
			
			if($('#txtDescricaoAddIngrediente').val() == null || $('#txtDescricaoAddIngrediente').val() == "")
			{
				retorno = false;
				mensagem += "- DIGITE A DESCRICAO DO INGREDIENTE\n";
			}
			
			if($('#cmbUnidadeMedidaIngrediente').val() == "")
			{
				retorno = false;
				mensagem += "- SELECIONE A UNIDADE DE MEDIDA\n";
			}
			
			if($('#txtQtdeInicialIngrediente').val() == null || 
					$('#txtQtdeInicialIngrediente').val() == "" ||
					isNaN($('#txtQtdeInicialIngrediente').val()))
			{
				retorno = false;
				mensagem += "- QTDE INICIAL INVÁLIDA";
			}
			
			if(!retorno)
			{
				alert("VERIFIQUE OS CAMPOS: \n\n" + mensagem);
			}
			
			return retorno;
			
		},
		retornaIngredienteDoForm: function(){
			
			var obj = {};
			
			obj.descricao = $('#txtDescricaoAddIngrediente').val();
			obj.unidadeMedida = clonarObjeto(modelo.listaUnidadesMedidas[parseInt($('#cmbUnidadeMedidaIngrediente').val())]);
			obj.qtdeInicial = $('#txtQtdeInicialIngrediente').val();
 			obj.tempoReposicao = $('#txtTempoReposicaoIngrediente').val();
			
			return obj;
		}
		
	};
	
	function retornaObjetoDadosSimulacao()
	{
		var obj = {};
		
		obj.horarioInicio = $('#txtHoraInicio').val();
		obj.horarioFim = $('#txtHoraFim').val();
		obj.qtdeClientes = $('#txtQtdeClientes').val();
		obj.listaIngredientes = clonarObjeto(modelo.listaIngredientes);
		obj.cardapio = clonarObjeto(modelo.cardapio);
		
		return obj
	}
	
	function clonarObjeto(objeto)
	{
		return JSON.parse(JSON.stringify(objeto));
	}
	
	function validaEnvio()
	{
		var mensagem = "";
		var retorno = true;
		
		if(!validaHora())
		{
			mensagem += "- DIGITE A HORA CORRETAMENTE\n";
			retorno = false;
		}
		
		if(modelo.listaIngredientes == null || modelo.listaIngredientes.length == 0)
		{
			mensagem += "- NENHUM INGREDIENTE FOI ADICIONADO\n";
			retorno = false;
		}
		
		if(modelo.cardapio == null || modelo.cardapio.length == 0)
		{
			mensagem += "- NENHUM PRATO FOI ADICIONADO AO CARDÁPIO\n";
			retorno = false;
		}
		
		if(!retorno)
		{
			alert("VERIFIQUE OS CAMPOS: \n\n" + mensagem);
		}
		
		return retorno;
	}
	
	function validaHora()
	{
		var retorno = true;
		var qtdeMinInicio = 0;
		var qtdeMinFim = 0;
		var vetAuxInicio = null;
		var vetAuxFim = null;
		
		if($('#txtHoraInicio').val() == null || $('#txtHoraInicio').val() == "")
		{
			retorno = false;
		}
		
		if($('#txtHoraFim').val() == null || $('#txtHoraFim').val() == "")
		{
			retorno = false;
		}
		
		vetAuxInicio = $('#txtHoraInicio').val().split(":");
		vetAuxFim = $("#txtHoraFim").val().split(":");
		
		qtdeMinInicio = parseInt(vetAuxInicio[1]);
		qtdeMinInicio += parseInt(vetAuxInicio[0]) * 60;
		console.log('qtde min inicio:' + qtdeMinInicio);
		
		qtdeMinFim = parseInt(vetAuxFim[1]);
		qtdeMinFim += parseInt(vetAuxFim[0]) * 60;
		console.log('qtde min fim:' + qtdeMinFim);
		
		if(qtdeMinInicio > qtdeMinFim)
		{
			retorno = false;
		}
		
		
		return retorno;
	}
	
</script>
</body>

</html>