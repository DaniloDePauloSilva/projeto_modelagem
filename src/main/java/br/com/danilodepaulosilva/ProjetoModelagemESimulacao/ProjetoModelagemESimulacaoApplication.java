package br.com.danilodepaulosilva.ProjetoModelagemESimulacao;

import java.util.ArrayList;
import java.util.List;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.web.servlet.support.SpringBootServletInitializer;

@SpringBootApplication
public class ProjetoModelagemESimulacaoApplication extends SpringBootServletInitializer{
	
	@Override
	protected SpringApplicationBuilder configure(SpringApplicationBuilder application) {
		return application.sources(ProjetoModelagemESimulacaoApplication.class);
	}

	public static void main(String[] args) {
		SpringApplication.run(ProjetoModelagemESimulacaoApplication.class, args);
	}

}
