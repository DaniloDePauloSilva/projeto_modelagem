package br.com.danilodepaulosilva.ProjetoModelagemESimulacao.controller;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.fasterxml.jackson.databind.ObjectMapper;

import br.com.danilodepaulosilva.ProjetoModelagemESimulacao.dto.Cliente;
import br.com.danilodepaulosilva.ProjetoModelagemESimulacao.dto.DadosSimulacao;
import br.com.danilodepaulosilva.ProjetoModelagemESimulacao.dto.Evento;
import br.com.danilodepaulosilva.ProjetoModelagemESimulacao.dto.Ingrediente;
import br.com.danilodepaulosilva.ProjetoModelagemESimulacao.dto.IngredienteDoPrato;
import br.com.danilodepaulosilva.ProjetoModelagemESimulacao.dto.QtdeClientesEmFila;
import br.com.danilodepaulosilva.ProjetoModelagemESimulacao.dto.Prato;
import br.com.danilodepaulosilva.ProjetoModelagemESimulacao.dto.RetornoSimulacao;

@Controller
public class ControllerPrincipal {

	@RequestMapping("/")
	public ModelAndView teste() 
	{
		return new ModelAndView("formulario");
	}
	

	@RequestMapping(value = "/simulacao", method = RequestMethod.POST, consumes=MediaType.APPLICATION_JSON_VALUE, produces=MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public RetornoSimulacao simulacao(@RequestBody String strDadosSimulacao) throws Exception
	{
		System.out.println("String corpo post:");
		System.out.println(strDadosSimulacao);
		DadosSimulacao dadosSimulacao = new ObjectMapper().readValue(strDadosSimulacao, DadosSimulacao.class);
		
		System.out.println("DADOS SIMULACAO: ");
		System.out.println("Horário início: " + dadosSimulacao.getHorarioInicio());
		System.out.println("Horário fim: " + dadosSimulacao.getHorarioFim());
		System.out.println("Quantidade de clientes no dia: " + dadosSimulacao.getQtdeClientes());
		
		System.out.println("");
		System.out.println("	- Ingredientes: ");
		for(Ingrediente i : dadosSimulacao.getListaIngredientes())
		{
			System.out.println("");
			System.out.println("descricao: " + i.getDescricao());
			System.out.println("unidade de medida: " + i.getUnidadeMedida().getDescricao() + "( " + i.getUnidadeMedida().getSigla() + " )");
			System.out.println("quantidade inicial: " + i.getQtdeInicial() + " " + i.getUnidadeMedida().getSigla());
			System.out.println("tempo de reposição: " + i.getTempoReposicao());
		}
		
		System.out.println("");
		System.out.println("	- Pratos: ");
		for(Prato p : dadosSimulacao.getCardapio())
		{
			System.out.println("");
			System.out.println("descrição: " + p.getDescricao());
			System.out.println("ingredientes: ");
			
			for(IngredienteDoPrato i : p.getListaIngredientes())
			{
				System.out.println("	descrição: " + i.getDescricao());
				System.out.println("	qtde: " + i.getQtde() + " " + i.getUnidadeMedida().getSigla());
			}
		}
		
		
		RetornoSimulacao retorno = iniciarSimulacao(dadosSimulacao);
		
		return retorno;
	}
	
	private Double retornaMediaTempoFila(List<Cliente> clientesAtendidos)
	{
		Double soma = 0.0;
		
		for(Cliente c : clientesAtendidos)
		{
			soma += c.getMinutoDeAtendimento() - c.getMinutoDeChegada();
		}
		
		return soma/clientesAtendidos.size();
	}
	
	private List<IngredienteDoPrato> retornaListaIngredienteSaida(List<Ingrediente> listaIngrediente)
	{
		List<IngredienteDoPrato> lista = new ArrayList<>();
		
		for(Ingrediente i : listaIngrediente)
		{
			lista.add(i.retornaIngredienteSaida());
		}
		
		return lista;
		
	}
	
	private RetornoSimulacao iniciarSimulacao(DadosSimulacao dadosSimulacao) 
	{
		RetornoSimulacao retorno = new RetornoSimulacao();
		
		List<Evento> listaEventos = new ArrayList<>();
		List<Cliente> listaClientes = escalonarEventos(dadosSimulacao);
		
		List<IngredienteDoPrato> listaIngredientesSaida = retornaListaIngredienteSaida(dadosSimulacao.getListaIngredientes());
		
		List<Cliente> fila = new ArrayList<>();
		List<Cliente> clientesAtendidos = new ArrayList<>();
		
		Integer instante = 0;
		Integer instanteFinal = dadosSimulacao.retornaQuantidadeDeMinutosDaSimulacao();
		
		Cliente clienteEmAtendimento = null;
		Integer tempoCorrentePratoEmPreparo = 0;
		QtdeClientesEmFila max = new QtdeClientesEmFila(0,0);
		List<QtdeClientesEmFila> qtdeClientesEmFila = new ArrayList<>();
		boolean preparandoLanche = false;
		
		
		while (instante < instanteFinal || (preparandoLanche) || fila.size() > 0)
		{
			qtdeClientesEmFila.add(new QtdeClientesEmFila(fila.size(), instante));
			System.out.println("\nINSTANTE: " + instante + "\n");
			
			List<Cliente> clientesDoInstante = verificarSeChegouCliente(instante, listaClientes);
			
			//SE CHEGOU CLIENTE, ENTRA NA FILA
			if(clientesDoInstante != null)
			{
				for(Cliente c: clientesDoInstante)
				{
					fila.add(c);
					listaEventos.add(new Evento(instante, "CLIENTE-" + c.getId() + " ENTROU NA FILA"));
					System.out.println(listaEventos.get(listaEventos.size() - 1).getDescricao());
					
					if(fila.size() > max.getQtde())
					{
						max.set(fila.size(), instante);
					}
				}
				
				System.out.println("\nFILA: ");
				for(int i = 0; i < fila.size(); i++)
				{
					System.out.println("cliente-" + fila.get(i).getId());
				}
				System.out.println();
			}
			
			if(!preparandoLanche)
			{
				if(fila.size() > 0)
				{
					clienteEmAtendimento = fila.remove(0);
					
					listaEventos.add(new Evento(instante, "CLIENTE-" + clienteEmAtendimento.getId() + " SAIU DA FILA"));
					System.out.println(listaEventos.get(listaEventos.size() - 1).getDescricao());
					
					System.out.println("\nFILA: ");
					for(int i = 0; i < fila.size(); i++)
					{
						System.out.println("cliente-" + fila.get(i).getId());
					}
					System.out.println();
					
					clienteEmAtendimento.setMinutoDeAtendimento(instante);
					tempoCorrentePratoEmPreparo = 0;
					preparandoLanche = true;
					subtrairIngredientes(clienteEmAtendimento.getPedido(), listaIngredientesSaida);
					
					listaEventos.add(new Evento(instante, "INICIOU PREPARO DO PEDIDO DO CLIENTE-" + clienteEmAtendimento.getId() + 
							" - " + clienteEmAtendimento.getPedido().getDescricao()));
					System.out.println(listaEventos.get(listaEventos.size() - 1).getDescricao());
				}
				else 
				{
//					listaEventos.add(new Evento(instante, "OCIOSO"));
//					System.out.println(listaEventos.get(listaEventos.size() - 1).getDescricao());
				}
			}
			else 
			{
				tempoCorrentePratoEmPreparo++;
				
				if(tempoCorrentePratoEmPreparo >= clienteEmAtendimento.getPedido().getTempoPreparo())
				{
					clientesAtendidos.add(clienteEmAtendimento);
					tempoCorrentePratoEmPreparo = 0;
					preparandoLanche = false;
					
					listaEventos.add(new Evento(instante, "PEDIDO DO CLIENTE-" + clienteEmAtendimento.getId() + " PRONTO"));
					System.out.println(listaEventos.get(listaEventos.size() - 1).getDescricao());
					
					clienteEmAtendimento = null;
				}
				else 
				{
					preparandoLanche = true;
					listaEventos.add(new Evento(instante, "PREPARANDO PEDIDO DO CLIENTE-" + clienteEmAtendimento.getId()));
					System.out.println(listaEventos.get(listaEventos.size() - 1).getDescricao());
				}
			}
			
			instante++;
		}
		
		retorno.setTamanhoMaximoDaFila(max);
		retorno.setListaQtdeClientesEmFila(qtdeClientesEmFila);
		retorno.setMediaTempoEmFila(retornaMediaTempoFila(clientesAtendidos));
		retorno.setClientesAtendidos(clientesAtendidos);
		retorno.setListaIngredientesSaida(listaIngredientesSaida);
		retorno.setListaEventos(listaEventos);
		
		return retorno;
	}
	
	private void subtrairIngredientes(Prato prato, List<IngredienteDoPrato> listaIngrediente) 
	{
		for(int i = 0; i < listaIngrediente.size(); i++)
		{
			for(int j = 0; j < prato.getListaIngredientes().size(); j++)
			{
				if(prato.getListaIngredientes().get(j).getDescricao().equals(listaIngrediente.get(i).getDescricao()))
				{
					int qtde = listaIngrediente.get(i).getQtde();
					qtde -= prato.getListaIngredientes().get(j).getQtde();
					listaIngrediente.get(i).setQtde(qtde);
					break;
				}
			}	
		}
	}
	
	private List<Cliente> verificarSeChegouCliente(Integer instante, List<Cliente> listaClientes) 
	{
		List<Cliente> retorno = new ArrayList<>();
		
		for(int i = 0; i < listaClientes.size(); i++)
		{
			if(listaClientes.get(i).getMinutoDeChegada().equals(instante))
			{
				Cliente c = listaClientes.remove(i);
				retorno.add(c);
				i--;
			}
		}
		
		return retorno.size() == 0 ? null: retorno;
	}
	
	private List<Cliente> escalonarEventos(DadosSimulacao ds)
	{
		List<Cliente> listaCliente = new ArrayList<>();
		
		Integer tempoTotal = ds.retornaQuantidadeDeMinutosDaSimulacao();
		Integer qtdeClientes = ds.getQtdeClientes();
		System.out.println("tempo total: " + tempoTotal);
		
		
		List<Integer> temposDeChegada = temposDeChegada(tempoTotal, qtdeClientes);
		
		for(int i = 0; i < temposDeChegada.size(); i++)
		{
			Cliente cliente = new Cliente(temposDeChegada.get(i));
			cliente.setId(i + 1);
			cliente.setPedido(ds.sortearPrato());
			
			listaCliente.add(cliente);
		}
		
		
		return listaCliente;
	}
	
	public List<Integer> temposDeChegada (Integer tempoTotal, Integer qtdeClientes) 
	{
		List<Integer> instantesChegada = new ArrayList<>();
		List<Double> listaProb = probabilidade(tempoTotal);
		
		while(instantesChegada.size() < qtdeClientes)
		{
			for(int i = 0; i < listaProb.size(); i++) 
			{
				Double valSort = Math.random();
				
				if(valSort < listaProb.get(i))
				{
					instantesChegada.add(i);
				}
				
				if(instantesChegada.size() == qtdeClientes)
					break;
			}
		}
		
		Collections.sort(instantesChegada);
		
		for(int i = 0; i < instantesChegada.size(); i++)
		{
			int x = instantesChegada.get(i);
			System.out.println(i + ") " + x);
		}
		
		return instantesChegada;
	}
	
	public List<Double> probabilidade(Integer tempoTotal)
	{
		Double totalMinutos = tempoTotal.doubleValue();
		
		List<Double> lista = new ArrayList<>();
		
		for(Double i = 0.0; i < totalMinutos; i++)
		{
			Double fx = distribuicaoNormal(i, (totalMinutos)/2, 50.0);
			System.out.println(i +")  " + (fx));
			
			lista.add(fx);
		}
		
		Double somatoria = 0.0;
		
		for(Double d : lista)
			somatoria += d;
		
		System.out.println("SOMATORIA: " + somatoria);
		
		
		return lista;
	}
	
	private static Double distribuicaoNormal(Double x, Double media, Double desvioPadrao) 
	{
		Double A = (1.0/((desvioPadrao) * Math.sqrt(2.0 * Math.PI)));
		
		Double B = (-1.0/2.0) * Math.pow(((x - media)/desvioPadrao), 2);
		
		Double C = Math.pow(Math.E, B);
		
		return A * C;
	}
	
	
	
	public void verificarCliente(List<Cliente> listaCliente, DadosSimulacao ds, Integer i, Integer metadeSimulacao) 
	{
		
	}
	
	private Double retornaProbabilidadeDeChegada(DadosSimulacao ds, Integer instante) 
	{
		
		
		return 0.0;
	}
	
}
