package br.com.danilodepaulosilva.ProjetoModelagemESimulacao.dto;

import java.util.List;

public class DadosSimulacao {
	
	private String horarioInicio;
	private String horarioFim;
	private Integer qtdeClientes;
	List<Ingrediente> listaIngredientes;
	List<Prato> cardapio;
	public String getHorarioInicio() {
		return horarioInicio;
	}
	public void setHorarioInicio(String horarioInicio) {
		this.horarioInicio = horarioInicio;
	}
	public String getHorarioFim() {
		return horarioFim;
	}
	public void setHorarioFim(String horarioFim) {
		this.horarioFim = horarioFim;
	}
	public Integer getQtdeClientes() {
		return qtdeClientes;
	}
	public void setQtdeClientes(Integer qtdeClientes) {
		this.qtdeClientes = qtdeClientes;
	}
	public List<Ingrediente> getListaIngredientes() {
		return listaIngredientes;
	}
	public void setListaIngredientes(List<Ingrediente> listaIngredientes) {
		this.listaIngredientes = listaIngredientes;
	}
	public List<Prato> getCardapio() {
		return cardapio;
	}
	public void setCardapio(List<Prato> cardapio) {
		this.cardapio = cardapio;
	}
	
	public Integer retornaQuantidadeDeMinutosDaSimulacao() 
	{
		String[] v2 = getHorarioFim().split(":");
		String[] v1 = getHorarioInicio().split(":");
		
		Integer h2 = Integer.parseInt(v2[0]);
		Integer h1 = Integer.parseInt(v1[0]);
		Integer m2 = Integer.parseInt(v2[1]);
		Integer m1 = Integer.parseInt(v1[1]);
		
		
		return ((h2 * 60) + m2) - ((h1 * 60) + m1);
	}
	
	public Prato sortearPrato() 
	{
		Integer indicePrato = (int) (Math.random() * getCardapio().size());
		
		return getCardapio().get(indicePrato);
	}
	

}
