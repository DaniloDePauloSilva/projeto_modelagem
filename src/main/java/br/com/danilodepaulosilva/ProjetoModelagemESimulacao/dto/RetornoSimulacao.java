package br.com.danilodepaulosilva.ProjetoModelagemESimulacao.dto;

import java.util.List;

public class RetornoSimulacao {
	
	private String resposta;
	private Object obj;
	
	private Double mediaTempoEmFila;
	private List<Cliente> clientesAtendidos;
	private List<Evento> listaEventos;
	private List<QtdeClientesEmFila> listaQtdeClientesEmFila;
	private List<IngredienteDoPrato> listaIngredientesSaida;
	private QtdeClientesEmFila tamanhoMaximoDaFila;

	public String getResposta() {
		return resposta;
	}

	public void setResposta(String resposta) {
		this.resposta = resposta;
	}

	public Object getObj() {
		return obj;
	}

	public void setObj(Object obj) {
		this.obj = obj;
	}

	public List<Evento> getListaEventos() {
		return listaEventos;
	}

	public void setListaEventos(List<Evento> listaEventos) {
		this.listaEventos = listaEventos;
	}

	public List<Cliente> getClientesAtendidos() {
		return clientesAtendidos;
	}

	public void setClientesAtendidos(List<Cliente> clientesAtendidos) {
		this.clientesAtendidos = clientesAtendidos;
	}

	public Double getMediaTempoEmFila() {
		return mediaTempoEmFila;
	}

	public void setMediaTempoEmFila(Double mediaTempoEmFila) {
		this.mediaTempoEmFila = mediaTempoEmFila;
	}

	public QtdeClientesEmFila getTamanhoMaximoDaFila() {
		return tamanhoMaximoDaFila;
	}

	public void setTamanhoMaximoDaFila(QtdeClientesEmFila tamanhoMaximoDaFila) {
		this.tamanhoMaximoDaFila = tamanhoMaximoDaFila;
	}

	public List<QtdeClientesEmFila> getListaQtdeClientesEmFila() {
		return listaQtdeClientesEmFila;
	}

	public void setListaQtdeClientesEmFila(List<QtdeClientesEmFila> listaQtdeClientesEmFila) {
		this.listaQtdeClientesEmFila = listaQtdeClientesEmFila;
	}

	public List<IngredienteDoPrato> getListaIngredientesSaida() {
		return listaIngredientesSaida;
	}

	public void setListaIngredientesSaida(List<IngredienteDoPrato> listaIngredientesSaida) {
		this.listaIngredientesSaida = listaIngredientesSaida;
	}

}
