package br.com.danilodepaulosilva.ProjetoModelagemESimulacao.dto;

public class Cliente {
	
	private Integer id;
	private Integer minutoDeChegada;
	private Integer minutoDeAtendimento;
	private Integer minutoDeEntradaNaFila;
	private Prato pedido;
	
	
	
	public Cliente(Integer minutoDeChegada) 
	{
		this.minutoDeChegada = minutoDeChegada;
	}

	public Integer getMinutoDeChegada() {
		return minutoDeChegada;
	}

	public void setMinutoDeChegada(Integer minutoDeChegada) {
		
		
		
		this.minutoDeChegada = minutoDeChegada;
	}

	public Integer getMinutoDeAtendimento() {
		return minutoDeAtendimento;
	}

	public void setMinutoDeAtendimento(Integer minutoDeAtendimento) {
		this.minutoDeAtendimento = minutoDeAtendimento;
	}

	public Prato getPedido() {
		return pedido;
	}

	public void setPedido(Prato pedido) {
		this.pedido = pedido;
	}

	public Integer getMinutoDeEntradaNaFila() {
		return minutoDeEntradaNaFila;
	}

	public void setMinutoDeEntradaNaFila(Integer minutoDeEntradaNaFila) {
		this.minutoDeEntradaNaFila = minutoDeEntradaNaFila;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

}
