package br.com.danilodepaulosilva.ProjetoModelagemESimulacao.dto;

public class Ingrediente {

	private String descricao;
	private UnidadeMedida unidadeMedida;
	private Integer qtdeInicial;
	private Integer tempoReposicao;
	
	public String getDescricao() {
		return descricao;
	}
	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}
	public UnidadeMedida getUnidadeMedida() {
		return unidadeMedida;
	}
	public void setUnidadeMedida(UnidadeMedida unidadeMedida) {
		this.unidadeMedida = unidadeMedida;
	}
	public Integer getQtdeInicial() {
		return qtdeInicial;
	}
	public void setQtdeInicial(Integer qtdeInicial) {
		this.qtdeInicial = qtdeInicial;
	}
	public Integer getTempoReposicao() {
		return tempoReposicao;
	}
	public void setTempoReposicao(Integer tempoReposicao) {
		this.tempoReposicao = tempoReposicao;
	}
	
	public IngredienteDoPrato retornaIngredienteSaida() 
	{
		IngredienteDoPrato clone = new IngredienteDoPrato();
		
		clone.setDescricao(getDescricao());
		clone.setQtdeInicial(getQtdeInicial());
		clone.setUnidadeMedida(getUnidadeMedida());
		clone.setQtde(getQtdeInicial());
		
		return clone;
	}

	
}
