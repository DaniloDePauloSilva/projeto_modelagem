package br.com.danilodepaulosilva.ProjetoModelagemESimulacao.dto;

public class Evento {
	
	private Integer instante;
	private String descricao;
	
	public Evento(Integer instante, String descricao) 
	{
		setInstante(instante);
		setDescricao(descricao);
	}
	
	public Integer getInstante() {
		return instante;
	}
	public void setInstante(Integer instante) {
		this.instante = instante;
	}
	public String getDescricao() {
		return descricao;
	}
	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}
	
	

}
