package br.com.danilodepaulosilva.ProjetoModelagemESimulacao.dto;

public class QtdeClientesEmFila {
	
	private Integer qtde;
	private Integer instante;
	
	public QtdeClientesEmFila(Integer qtde, Integer instante) 
	{
		set(qtde, instante);
	}
	
	public void set(Integer qtde, Integer instante) 
	{
		setQtde(qtde);
		setInstante(instante);
	}
	
	public Integer getQtde() {
		return qtde;
	}
	public void setQtde(Integer qtde) {
		this.qtde = qtde;
	}
	public Integer getInstante() {
		return instante;
	}
	public void setInstante(Integer instante) {
		this.instante = instante;
	}
	
	

}
