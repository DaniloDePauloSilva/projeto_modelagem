package br.com.danilodepaulosilva.ProjetoModelagemESimulacao.dto;

public class IngredienteDoPrato extends Ingrediente{

	private Integer qtde;
	
	public Integer getQtde() {
		return qtde;
	}
	public void setQtde(Integer qtde) {
		this.qtde = qtde;
	}
	
}
