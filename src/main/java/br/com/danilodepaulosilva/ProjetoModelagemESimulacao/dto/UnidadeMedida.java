package br.com.danilodepaulosilva.ProjetoModelagemESimulacao.dto;

public class UnidadeMedida {
	
	private String descricao;
	private String sigla;
	public String getDescricao() {
		return descricao;
	}
	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}
	public String getSigla() {
		return sigla;
	}
	public void setSigla(String sigla) {
		this.sigla = sigla;
	}
	
	

}
