package br.com.danilodepaulosilva.ProjetoModelagemESimulacao.dto;

import java.util.List;

public class Prato {
	
	private String descricao;
	private Integer tempoPreparo;
	private List<IngredienteDoPrato> listaIngredientes;
	
	public String getDescricao() {
		return descricao;
	}
	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}
	public Integer getTempoPreparo() {
		return tempoPreparo;
	}
	public void setTempoPreparo(Integer tempoPreparo) {
		this.tempoPreparo = tempoPreparo;
	}
	public List<IngredienteDoPrato> getListaIngredientes() {
		return listaIngredientes;
	}
	public void setListaIngredientes(List<IngredienteDoPrato> listaIngredientes) {
		this.listaIngredientes = listaIngredientes;
	}
	
	

}
