package br.com.danilodepaulosilva.ProjetoModelagemESimulacao;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import org.junit.jupiter.api.Test;

public class TesteDistribuicao {
	
	@Test
	public void testeDistribuicao_3() 
	{
		List<Integer> instantesChegada = new ArrayList<>();
		List<Double> listaProb = probabilidade();
		
		while(instantesChegada.size() < 50)
		{
			for(int i = 0; i < listaProb.size(); i++) 
			{
				Double valSort = Math.random();
				
				if(valSort < listaProb.get(i))
				{
					instantesChegada.add(i);
				}
				
				if(instantesChegada.size() == 50)
					break;
			}
		}
		
		Collections.sort(instantesChegada);
		
		for(int i = 0; i < instantesChegada.size(); i++)
		{
			int x = instantesChegada.get(i);
			System.out.println(i + ") " + x);
		}
	}
	
	public List<Double> probabilidade()
	{
		Double totalMinutos = 540.0;
		
		List<Double> lista = new ArrayList<>();
		
		for(Double i = 0.0; i < totalMinutos; i++)
		{
			Double fx = distribuicaoNormal(i, (totalMinutos)/2, 40.0);
			//System.out.println(i +")  " + (fx));
			
			lista.add(fx);
		}
		
		Double somatoria = 0.0;
		
		for(Double d : lista)
			somatoria += d;
		
		System.out.println("SOMATORIA: " + somatoria);
		
		
		return lista;
	}
	
	//@Test
	public void testeDistribuicao_2() 
	{
		Double totalMinutos = 540.0;
		Double total = 100.0;
		Double soma = 0.0;
		
		List<Double> lista = new ArrayList<>();
		
		Double div = distribuicaoNormal(totalMinutos/2, (totalMinutos)/2, 20.0);
		
		for(Double i = 0.0; i < totalMinutos; i++)
		{
			Double fx = distribuicaoNormal(i, (totalMinutos)/2, 20.0);
			System.out.println(i +")  " + (fx));
			
			lista.add(fx/div);
		}
		
		for(int i = 0; i < lista.size(); i++)
		{
			Double d = lista.get(i);
			soma += d;
			System.out.println(i + ") lista: " + d);
		}
		
		System.out.println("soma: " + soma);
		
	}
	
	//@Test
	public void testeDistribuicao_1() 
	{
		List<Double> valores = new ArrayList<>();
		
		Double total = 100.0;
		Double tempoTotal = 540.0;
		
		for(Double i = 0.0; i < total; i++)
		{
			Double fx = distribuicaoNormal(i, (total)/2, 30.0);
			valores.add(fx);
			//System.out.println("x = " + i);
			System.out.println("f(x) = " + fx);
			
			
			
		}
		
		Long somatoria = (long) 0;
		
		for(int i = 1; i < valores.size(); i++)
		{
			Double var1 = (1 / valores.get(i - 1));
			Double var2 = (1 / valores.get(i));
			
			Long dif = Math.round(var1) - Math.round(var2);
			
			somatoria += Math.abs(dif);
			
			System.out.println("dif: " + dif);
			//System.out.println("soma: " + somatoria);
		}
		
		System.out.println("valores.size(): " + valores.size());
		
	}
	
	private static Double distribuicaoNormal(Double x, Double media, Double desvioPadrao) 
	{
		Double A = (1.0/((desvioPadrao) * Math.sqrt(2.0 * Math.PI)));
		
		Double B = (-1.0/2.0) * Math.pow(((x - media)/desvioPadrao), 2);
		
		Double C = Math.pow(Math.E, B);
		
		return A * C;
	}

}
